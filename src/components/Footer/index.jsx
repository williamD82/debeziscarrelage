import React from "react";
import { APP_NAME } from "../../commons/constants";
import { CiFacebook, CiLinkedin } from "react-icons/ci";
import { FiInstagram } from "react-icons/fi";
import { RiTwitterXFill } from "react-icons/ri";
import Theme from "../../styles/theme";
import styled from "styled-components";

const FooterContainer = styled.footer`
  background-color: ${Theme.colors.primary.default};
  padding: 40px 20px;
  color: ${Theme.colors.white};
  text-align: center;
`;

const FooterContent = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  align-items: center;
  gap: 20px;

  @media (max-width: 768px) {
    flex-direction: column;
  }
`;

const Logo = styled.div`
  font-size: ${Theme.fontSize.h4}px;
  font-weight: bold;
`;

const SocialLinks = styled.div`
  display: flex;
  gap: 20px;

  a {
    color: ${Theme.colors.white};
    font-size: ${Theme.fontSize.xlarge}px;
    transition: color 0.3s;

    &:hover {
      color: ${Theme.colors.accent.default};
    }
  }
`;

const LogoTitle = styled.div`
  text-decoration: none;
  color: ${Theme.colors.white};
  font-size: ${Theme.fontSize.regular}px;
  text-align: center;
`;

const Title = styled.p`
  text-decoration: none;
  color: ${Theme.colors.white};
  font-size: ${Theme.fontSize.regular}px;
`;

const Nav = styled.nav`
  display: flex;
  gap: 20px;

  @media (max-width: 768px) {
    flex-direction: column;
    align-items: center;
  }
`;

const NavLink = styled.a`
  text-decoration: none;
  color: ${Theme.colors.white};
  font-size: ${Theme.fontSize.regular}px;
  transition: color 0.3s;

  &:hover {
    color: ${Theme.colors.accent.default};
  }
`;

const Copyright = styled.p`
  font-size: ${Theme.fontSize.small}px;
  margin-top: 20px;
  color: ${Theme.colors.gray500};
`;

const Footer = () => {
  return (
    <FooterContainer>
      <FooterContent>
        <LogoTitle>
          <Logo>{APP_NAME}</Logo>
          <Title>CARRELEUR - MOSAISTE</Title>
        </LogoTitle>
        <SocialLinks>
          <a
            href="https://www.facebook.com"
            target="_blank"
            rel="noopener noreferrer"
          >
            <CiFacebook />
          </a>
          <a
            href="https://www.instagram.com"
            target="_blank"
            rel="noopener noreferrer"
          >
            <FiInstagram />
          </a>
          <a
            href="https://www.twitter.com"
            target="_blank"
            rel="noopener noreferrer"
          >
            <RiTwitterXFill />
          </a>
          <a
            href="https://www.linkedin.com"
            target="_blank"
            rel="noopener noreferrer"
          >
            <CiLinkedin />
          </a>
        </SocialLinks>
        <Nav>
          <NavLink href="#accueil">Accueil</NavLink>
          <NavLink href="#histoire">Histoire</NavLink>
          <NavLink href="#services">Services</NavLink>
          <NavLink href="#projets">Projets</NavLink>
          <NavLink href="#contact">Contact</NavLink>
        </Nav>
      </FooterContent>
      <Copyright>&copy; 2024 {APP_NAME}. Tous droits réservés.</Copyright>
    </FooterContainer>
  );
};

export default Footer;
