import React from "react";
import styled from "styled-components";
import Theme from "../../styles/theme";
import { FaFileAlt, FaHandshake } from "react-icons/fa";
import { HiOutlineRectangleGroup } from "react-icons/hi2";

const ServicesContainer = styled.section`
  padding: 60px 20px;
  background-color: ${Theme.colors.gray800};
  text-align: center;
`;

const Title = styled.h2`
  font-size: ${Theme.fontSize.h2}px;
  color: ${Theme.colors.white};
  margin-bottom: 40px;
`;

const Divider = styled.div`
  width: 50px;
  height: 3px;
  background-color: ${Theme.colors.accent.default};
  margin: 0 auto 20px auto;
`;

const ServiceList = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
  gap: 20px;
`;

const ServiceItem = styled.div`
  flex: 1;
  min-width: 250px;
  max-width: 300px;
  margin: 20px;
  text-align: center;
`;

const ServiceIcon = styled.div`
  font-size: 4em;
  color: ${Theme.colors.accent.default};
  margin-bottom: 20px;
  border: 2px solid ${Theme.colors.white};
  border-radius: 50%;
  width: 100px;
  height: 100px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0 auto 20px auto;
`;

const ServiceTitle = styled.h3`
  font-size: ${Theme.fontSize.xlarge}px;
  color: ${Theme.colors.white};
  margin-bottom: 10px;
`;

const ServiceDescription = styled.p`
  font-size: ${Theme.fontSize.regular}px;
  color: ${Theme.colors.gray400};
`;

const Button = styled.a`
  display: inline-block;
  margin-top: 40px;
  padding: 10px 20px;
  font-size: ${Theme.fontSize.regular}px;
  color: ${Theme.colors.white};
  background-color: ${Theme.colors.accent.default};
  text-decoration: none;
  border-radius: 5px;
  &:hover {
    background-color: ${Theme.colors.accent.dark};
  }
`;

const Services = () => {
  return (
    <ServicesContainer id="services">
      <Title>SERVICES</Title>
      <Divider />
      <ServiceList>
        <ServiceItem>
          <ServiceIcon>
            <FaFileAlt />
          </ServiceIcon>
          <ServiceTitle>DEVIS GRATUIT</ServiceTitle>
          <ServiceDescription>
            Nous venons à domicile pour établir gratuitement un devis sur
            mesure.
          </ServiceDescription>
        </ServiceItem>
        <ServiceItem>
          <ServiceIcon>
            <HiOutlineRectangleGroup />
          </ServiceIcon>
          <ServiceTitle>REVÊTEMENT</ServiceTitle>
          <ServiceDescription>
            Spécialisés dans la pose de carrelage et mosaïque. Nous proposons
            tous types de revêtements sur sols et murs.
          </ServiceDescription>
        </ServiceItem>
        <ServiceItem>
          <ServiceIcon>
            <FaHandshake />
          </ServiceIcon>
          <ServiceTitle>COORDINATION</ServiceTitle>
          <ServiceDescription>
            Coordination de tous vos besoins dans l'élaboration de votre
            chantier grâce à une équipe de professionnels, chacun expert dans
            son domaine.
          </ServiceDescription>
        </ServiceItem>
      </ServiceList>
      <Button href="#contact">DEVIS GRATUIT</Button>
    </ServicesContainer>
  );
};

export default Services;
