import React from "react";
import { useForm } from "react-hook-form";
import emailjs from "emailjs-com";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import styled from "styled-components";
import Theme from "../../styles/theme";
import formImage from "../../assets/images/form.jpg";
import { GiPositionMarker } from "react-icons/gi";
import { BsFillTelephoneFill } from "react-icons/bs";
import { IoMdMail } from "react-icons/io";

const ContactContainer = styled.section`
  padding: 60px 20px;
  background-image: url(${formImage});
  background-size: cover;
  background-position: center;
  text-align: center;
`;

const Title = styled.h2`
  font-size: ${Theme.fontSize.h2}px;
  color: ${Theme.colors.white};
  margin-bottom: 20px;
  background-color: ${Theme.colors.accent.dark};
  border-radius: 5px;

  @media (max-width: 768px) {
    background-color: transparent;
  }
`;

const Divider = styled.div`
  width: 50px;
  height: 3px;
  background-color: ${Theme.colors.accent.default};
  margin: 0 auto 40px auto;
`;

const FormWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  align-items: flex-start;
  flex-wrap: wrap;
`;

const ContactInfo = styled.div`
  max-width: 400px;
  text-align: left;
  margin-bottom: 20px;
  color: ${Theme.colors.white};

  @media (max-width: 768px) {
    text-align: center;
    margin-bottom: 40px;
    background-color: ${Theme.colors.accent.dark};
    border-radius: 5px;
    padding: ${Theme.padding.regular}px;
  }
`;

const InfoItem = styled.p`
  font-size: ${Theme.fontSize.regular}px;
  margin-bottom: 10px;
  font-weight: bold;
  display: flex;
  align-items: center;
  gap: 5px;
`;

const InfoTitle = styled.p`
  font-size: ${Theme.fontSize.xlarge}px;
  font-weight: bold;
`;

const Form = styled.form`
  max-width: 400px;
  width: 100%;
  display: flex;
  flex-direction: column;
  background-color: rgba(0, 0, 0, 0.6);
  padding: 20px;
  border-radius: 10px;
`;

const Input = styled.input`
  font-size: ${Theme.fontSize.regular}px;
  padding: 10px;
  margin-bottom: 10px;
  width: 100%;
  border: 1px solid ${Theme.colors.gray300};
  border-radius: 5px;
  box-sizing: border-box;
  background-color: ${Theme.colors.white};
`;

const TextArea = styled.textarea`
  font-size: ${Theme.fontSize.regular}px;
  padding: 10px;
  margin-bottom: 10px;
  width: 100%;
  border: 1px solid ${Theme.colors.gray300};
  border-radius: 5px;
  height: 100px;
  box-sizing: border-box;
  background-color: ${Theme.colors.white};
`;

const Button = styled.button`
  font-size: ${Theme.fontSize.regular}px;
  color: ${Theme.colors.white};
  background-color: ${Theme.colors.accent.default};
  padding: 10px 20px;
  border: none;
  border-radius: 5px;
  cursor: pointer;
  &:hover {
    background-color: ${Theme.colors.accent.dark};
  }
`;

const ErrorMessage = styled.span`
  color: ${Theme.colors.error.default};
  font-size: ${Theme.fontSize.small}px;
  margin-bottom: 10px;
  display: block;
`;

const notifySuccess = (name) => {
  toast.success(`Email envoyé avec succès, ${name}!`, {
    position: "top-right",
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
    theme: "colored",
  });
};

const notifyError = () => {
  toast.error("Erreur lors de l'envoi de l'email", {
    position: "top-right",
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
    theme: "colored",
  });
};

const ContactForm = () => {
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm();

  const onSubmit = (data) => {
    emailjs
      .send(
        "service_e8l6dvd",
        "template_y8g756j",
        {
          to_name: "Debezis Carrelage",
          from_name: data.name,
          name: data.name,
          email: data.email,
          phone: data.phone,
          address: data.address,
          message: data.message,
        },
        "YvlmvME_CzrheE_g7"
      )
      .then(
        (result) => {
          // console.log(result.text);
          notifySuccess(data.name);
          reset();
        },
        (error) => {
          // console.log(error.text);
          notifyError();
        }
      );
  };

  return (
    <ContactContainer id="contact">
      <Title>DEMANDE DE DEVIS GRATUIT</Title>
      <Divider />
      <FormWrapper>
        <ContactInfo>
          <InfoItem>
            <GiPositionMarker style={{ marginRight: "8px" }} />
            Sainte Marie La Mer
          </InfoItem>
          <InfoItem>
            <IoMdMail style={{ marginRight: "8px" }} />
            <a
              href="mailto:debeziscarrelage@gmail.com"
              style={{
                color: "inherit",
                textDecoration: "none",
                display: "flex",
                alignItems: "center",
              }}
            >
              debeziscarrelage@gmail.com
            </a>
          </InfoItem>
          <InfoItem>
            <BsFillTelephoneFill style={{ marginRight: "8px" }} />
            <a
              href="tel:+3372019134"
              style={{ color: "inherit", textDecoration: "none" }}
            >
              06 72 01 91 34
            </a>
          </InfoItem>
          <InfoTitle>Être rappelé gratuitement</InfoTitle>
        </ContactInfo>
        <Form onSubmit={handleSubmit(onSubmit)}>
          <Input
            {...register("name", { required: "Nom est requis" })}
            placeholder="Nom *"
          />
          {errors.name && <ErrorMessage>{errors.name.message}</ErrorMessage>}

          <Input
            {...register("email", {
              required: "Email est requis",
              pattern: {
                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                message: "Adresse email invalide",
              },
            })}
            placeholder="Email *"
          />
          {errors.email && <ErrorMessage>{errors.email.message}</ErrorMessage>}

          <Input
            {...register("phone", { required: "Téléphone est requis" })}
            placeholder="Téléphone *"
          />
          {errors.phone && <ErrorMessage>{errors.phone.message}</ErrorMessage>}

          <Input
            {...register("address", { required: "Adresse est requise" })}
            placeholder="Adresse *"
          />
          {errors.address && (
            <ErrorMessage>{errors.address.message}</ErrorMessage>
          )}

          <TextArea {...register("message")} placeholder="Message" />

          <Button type="submit">Envoyer</Button>
        </Form>
      </FormWrapper>
      <ToastContainer />
    </ContactContainer>
  );
};

export default ContactForm;
