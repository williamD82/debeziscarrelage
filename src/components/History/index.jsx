import React from "react";
import { APP_NAME } from "../../commons/constants";
import styled from "styled-components";
import Theme from "../../styles/theme";

const HistoryContainer = styled.section`
  padding: 60px 20px;
  background-color: ${Theme.colors.gray200};
  text-align: center;
`;

const Title = styled.h2`
  font-size: ${Theme.fontSize.h2}px;
  color: ${Theme.colors.primary.default};
  margin-bottom: 20px;
  position: relative;

  &:after {
    content: "";
    display: block;
    width: 50px;
    height: 3px;
    background-color: ${Theme.colors.accent.default};
    margin: 10px auto 0;
  }
`;

const ContentWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  gap: 20px;
  margin-top: 40px;
`;

const TextWrapper = styled.div`
  flex: 2;
  min-width: 300px;
  max-width: 600px;
  text-align: left;
  padding: 20px;
  background-color: ${Theme.colors.white};
  border-radius: 10px;
`;

const Text = styled.p`
  font-size: ${Theme.fontSize.xlarge}px;
  color: ${Theme.colors.text.default};
  line-height: 1.6;
`;

const History = () => {
  return (
    <HistoryContainer id="histoire">
      <Title>Notre Histoire</Title>
      <ContentWrapper>
        <TextWrapper>
          <Text>
            Depuis 2009, {APP_NAME}  transforme les espaces de vie et de travail avec
            passion et expertise. Forts de plus de dix ans d'expérience, nous
            avons perfectionné l'art de la pose de carrelage en combinant
            savoir-faire traditionnel et techniques modernes. Chaque projet est
            une opportunité de montrer notre dévouement à l'excellence, en
            utilisant des matériaux de haute qualité pour créer des espaces
            uniques et durables. Nous nous engageons à dépasser les attentes de
            nos clients en offrant des solutions sur mesure et un service
            impeccable.
          </Text>
        </TextWrapper>
      </ContentWrapper>
    </HistoryContainer>
  );
};

export default History;
