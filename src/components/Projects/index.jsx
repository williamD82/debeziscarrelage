import React from "react";
import styled from "styled-components";
import Theme from "../../styles/theme";
import { PROJECTS_DATA } from "../../commons/constants";

const ProjectsContainer = styled.section`
  padding: 60px 20px;
  background-color: ${Theme.colors.gray300};
  text-align: center;
`;

const Title = styled.h2`
  font-size: ${Theme.fontSize.h2}px;
  color: ${Theme.colors.primary.default};
  margin-bottom: 40px;
  position: relative;

  &:after {
    content: "";
    display: block;
    width: 50px;
    height: 3px;
    background-color: ${Theme.colors.accent.default};
    margin: 10px auto 0;
  }
`;

const ProjectList = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  gap: 20px;
`;

const ProjectItem = styled.div`
  background-color: ${Theme.colors.white};
  border-radius: 10px;
  overflow: hidden;
  width: 300px;
  transition: box-shadow 0.3s ease-in-out;

  &:hover {
    // box-shadow: ${Theme.colors.boxShadow.default};
    transform: scale(1.05);
  }
`;

const ProjectImage = styled.img`
  width: 100%;
  height: auto;
  border-bottom: 1px solid ${Theme.colors.gray400};
`;

const ProjectDescription = styled.div`
  padding: 20px;
`;

const ProjectTitle = styled.h3`
  font-size: ${Theme.fontSize.xlarge}px;
  color: ${Theme.colors.primary.dark};
  margin-bottom: 10px;
`;

const ProjectText = styled.p`
  font-size: ${Theme.fontSize.regular}px;
  color: ${Theme.colors.text.default};
`;

const Projects = () => {
  return (
    <ProjectsContainer id="projets">
      <Title>Nos Projets</Title>
      <ProjectList>
        {PROJECTS_DATA.map((project, index) => (
          <ProjectItem key={index}>
            <ProjectImage src={project.imageUrl} alt={project.title} />
            <ProjectDescription>
              <ProjectTitle>{project.title}</ProjectTitle>
              <ProjectText>{project.description}</ProjectText>
            </ProjectDescription>
          </ProjectItem>
        ))}
      </ProjectList>
    </ProjectsContainer>
  );
};

export default Projects;
