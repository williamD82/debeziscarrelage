import React from "react";
import styled from "styled-components";
import Theme from "../../styles/theme";
import heroImage from "../../assets/images/hero.jpg";
import { APP_NAME } from "../../commons/constants";
const HeroContainer = styled.div`
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background-image: url(${heroImage});
  background-size: cover;
  background-position: center;
  padding: 20px;
  text-align: center;
`;

const Title = styled.h1`
  font-size: ${Theme.fontSize.h1}px;
  color: ${Theme.colors.primary.default};
  margin-bottom: 40px;
`;

const Subtitle = styled.p`
  font-size: ${Theme.fontSize.xlarge}px;
  color: ${Theme.colors.text.light};
  margin-bottom: ${Theme.margin.regular * 6}px;
`;

const Button = styled.a`
  font-size: ${Theme.fontSize.regular}px;
  color: ${Theme.colors.white};
  background-color: ${Theme.colors.accent.default};
  padding: 10px 20px;
  text-decoration: none;
  border-radius: 5px;
  &:hover {
    background-color: ${Theme.colors.accent.dark};
  }
  @media (max-width: 768px) {
    padding: 20px 40px;
    font-size: ${Theme.fontSize.xlarge}px;
  }
`;

const HeroSection = () => {
  return (
    <HeroContainer id="accueil">
      <Title>
        {APP_NAME}
        <Subtitle>CARRELEUR - MOSAISTE</Subtitle>
      </Title>
      <Button href="#contact">Demandez un Devis Gratuit</Button>
    </HeroContainer>
  );
};

export default HeroSection;
