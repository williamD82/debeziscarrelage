import React, { useState } from "react";
import Theme from "../../styles/theme";
import styled from "styled-components";
import { FaBars } from "react-icons/fa";

const Container = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: ${Theme.colors.white};
  height: 70px;
  padding: 0 20px;
  box-shadow: ${Theme.colors.boxShadow.default};
  z-index: 1000;

  @media (max-width: 768px) {
    flex-direction: column;
    height: auto;
    align-items: flex-end;
  }
`;

const MenuToggle = styled.div`
  display: none;
  color: ${Theme.colors.primary.default};
  font-size: 2em;
  cursor: pointer;

  @media (max-width: 768px) {
    display: block;
    margin-right: ${Theme.margin.regular}px;
  }
`;

const Menu = styled.nav`
  display: flex;
  gap: 20px;

  @media (max-width: 768px) {
    flex-direction: column;
    align-items: flex-start;
    width: 100%;
    margin-bottom: ${Theme.margin.regular}px;
    margin-top: ${Theme.margin.regular}px;

    display: ${({ $isOpen }) => ($isOpen ? "flex" : "none")};
  }
`;

const MenuItem = styled.a`
  text-decoration: none;
  color: ${Theme.colors.text.default};
  font-size: ${Theme.fontSize.regular}px;
  font-weight: 500;

  &:hover {
    color: ${Theme.colors.accent.default};
  }
`;

const FabarsStyle = styled.div`
  margin: ${Theme.margin.regular}px;
`;

const Divider = styled.div`
  width: 50%;
  height: 3px;
  background-color: ${Theme.colors.accent.default};
`;
const Header = () => {
  const [isOpen, setIsOpen] = useState(false);

  const toggleMenu = () => {
    setIsOpen(!isOpen);
  };

  const closeMenu = () => {
    setIsOpen(false);
  };

  return (
    <Container>
      <MenuToggle onClick={toggleMenu}>
        <FabarsStyle>
          <FaBars />
        </FabarsStyle>
      </MenuToggle>
      <Menu $isOpen={isOpen}>
        <MenuItem href="#accueil" onClick={closeMenu}>
          ACCUEIL
        </MenuItem>
        <Divider />
        <MenuItem href="#histoire" onClick={closeMenu}>
          HISTOIRE
        </MenuItem>
        <Divider />
        <MenuItem href="#services" onClick={closeMenu}>
          SERVICES
        </MenuItem>
        <Divider />
        <MenuItem href="#projets" onClick={closeMenu}>
          PROJETS
        </MenuItem>
        <Divider />
        <MenuItem href="#contact" onClick={closeMenu}>
          CONTACT
        </MenuItem>
      </Menu>
    </Container>
  );
};

export default Header;
