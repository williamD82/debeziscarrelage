import React from "react";
import styled from "styled-components";
import Theme from "../../styles/theme";
import { TESTIMONIALS_DATA } from "../../commons/constants";
const TestimonialsContainer = styled.section`
  padding: 60px 20px;
  background-color: ${Theme.colors.gray100};
  text-align: center;
`;

const Title = styled.h2`
  font-size: ${Theme.fontSize.h2}px;
  color: ${Theme.colors.primary.default};
  margin-bottom: 40px;
`;

const Divider = styled.div`
  width: 50px;
  height: 3px;
  background-color: ${Theme.colors.accent.default};
  margin: 0 auto 40px auto;
`;

const TestimonialsList = styled.div`
  display: flex;
  justify-content: center;
  gap: 20px;

  @media (max-width: 768px) {
    display: flex;
    justify-content: center;
    gap: 20px;
    align-items: stretch;
    align-content: center;
    flex-wrap: wrap;
    flex-direction: column;
  }
`;

const TestimonialCard = styled.div`
  background-color: ${Theme.colors.white};
  border-radius: 10px;
  box-shadow: ${Theme.colors.boxShadow.default};
  padding: 20px;
  max-width: 300px;
  text-align: left;
`;

const Avatar = styled.img`
  border-radius: 50%;
  width: 60px;
  height: 60px;
  object-fit: cover;
  margin-bottom: 20px;
`;

const Name = styled.h3`
  font-size: ${Theme.fontSize.large}px;
  color: ${Theme.colors.primary.default};
  margin-bottom: 10px;
`;

const Quote = styled.p`
  font-size: ${Theme.fontSize.regular}px;
  color: ${Theme.colors.text.default};
  font-style: italic;
`;

const Testimonials = () => {
  return (
    <TestimonialsContainer id="testimonials">
      <Title>Ce que disent nos clients</Title>
      <Divider />
      <TestimonialsList>
        {TESTIMONIALS_DATA.map((testimonial, index) => (
          <TestimonialCard key={index}>
            <Avatar src={testimonial.avatarUrl} alt={testimonial.name} />
            <Name>{testimonial.name}</Name>
            <Quote>{testimonial.quote}</Quote>
          </TestimonialCard>
        ))}
      </TestimonialsList>
    </TestimonialsContainer>
  );
};

export default Testimonials;
