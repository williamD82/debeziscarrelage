import { Theme } from "../../styles/theme";
import { ThemeProvider } from "styled-components";
import { styled } from "styled-components";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import HeroSection from "../../components/HeroSection";
import Services from "../../components/Services";
// import Testimonials from "../../components/Testimonials";
import ContactForm from "../../components/ContactForm";
import History from "../../components/History";
import Projects from "../../components/Projects";

const Contenair = styled.div`
  font-family: ${Theme.fontFamily};

  @media (max-width: 768px) {
  }
`;
const HomeRoute = () => {
  return (
    <Contenair>
      <ThemeProvider theme={Theme}>
        <Header />
        <HeroSection />
        <History />
        <Services />
        <Projects />
        <ContactForm />
        {/* <Testimonials /> */}
        <Footer />
      </ThemeProvider>
    </Contenair>
  );
};

export default HomeRoute;
