import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomeRoute from "./Home";

export const Router = () => {
  return (
    <BrowserRouter basename="/">
      <Routes>
        <Route path="/" element={<HomeRoute />} />
      </Routes>
    </BrowserRouter>
  );
};

export default Router;
