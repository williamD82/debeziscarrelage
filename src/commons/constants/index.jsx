import sdb from "../../assets/images/sdb.jpg";
import cuisine from "../../assets/images/cuisine.jpg";
import terrasse from "../../assets/images/terrasse.jpg";
import JulienByrnt from "../../assets/images/JulienByrn't.jpg";
import LoïcNègre from "../../assets/images/LoicNegre.jpg";
import LucCosta from "../../assets/images/LucCosta.jpg";

export const APP_NAME = "Debezis Carrelage";
export const PROJECTS_DATA = [
  {
    title: "Salle de Bain Moderne",
    description:
      "Une salle de bain rénovée avec un carrelage moderne et élégant.",
    imageUrl: `${sdb}`,
  },
  {
    title: "Cuisine Contemporaine",
    description:
      "Une cuisine contemporaine avec des carreaux de haute qualité.",
    imageUrl: `${cuisine}`,
  },
  {
    title: "Terrasse Extérieure",
    description:
      "Une terrasse extérieure transformée avec un carrelage résistant et esthétique.",
    imageUrl: `${terrasse}`,
  },
];
export const TESTIMONIALS_DATA = [
  {
    name: "Julien Byrn't",
    quote: "Excellent travail, très professionnel !",
    avatarUrl: `${JulienByrnt}`,
  },
  {
    name: "Loïc Nègre",
    quote: "Le carrelage est magnifique et posé avec soin.",
    avatarUrl: `${LoïcNègre}`,
  },
  {
    name: "Luc Costa",
    quote: "Un service de qualité, je recommande vivement !",
    avatarUrl: `${LucCosta}`,
  },
];
